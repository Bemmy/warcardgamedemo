package warcardgamedemo;

import java.util.Scanner;

public class WarCardGameDemo {

    public static void main(String[] args) {
        
                Player player1 = new Player("Hanz Razzles", 0, 0);
                Player player2 = new Player("Ramses Trejo", 0, 0);
                
                WarCardGame warCardGame = new WarCardGame(player1, player2);
                
                playWar(warCardGame);
                
            }
    
            public static void playWar(WarCardGame warCardGame){
                
                Scanner in = new Scanner(System.in);
                
                char answer = 'Y';
                
                while( answer == 'Y'){
                warCardGame.playWarCardGame();
                
                    System.out.println("Play War Again? [ Yes/No] ");
                    
                    answer = in.next().charAt(0);
                    
                    answer = Character.toUpperCase(answer);
                }
            }
        }
    
