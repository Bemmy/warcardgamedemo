package warcardgamedemo;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Deck {
    
    private List<Card> cards;
    private List<Card> cardsWon;
    private List<Card> warHand;
    private Card card;

    public Deck(){
    
        cards = new ArrayList<Card>();
        
        for(CardSuit s : CardSuit.values()){
            for(CardValue v : CardValue.values()){
                Card card = new Card(s, v);
                cards.add(card); 
            }
        }
    }
    
    public Card getCard(){
        return card;
    }
    
    public void shuffleDeck(Deck deck){

        Collections.shuffle(cards);
        
        deck.setCards(cards);
    }
    
    public Deck(List<Card> cardsWon){
        this.cardsWon = cardsWon;
    }
    
    public List<Card> getCards(){
        return cards;
    }
    
    public void setCards(List<Card> cards){
        this.cards = cards;
    }
    
    public Deck createWinningsDeck(){
        cardsWon = new ArrayList<Card>();
        
        return new Deck(cardsWon);
    }
    
    public List<Card> getCardsWon(){
        return cardsWon;
    }
    
    public List<Card> getWarHand(){
        return warHand;
    }
    
    public void setWarHand(List<Card> warHand){
        this.warHand = warHand;
    }
    
    public String toString(){
        
        return "Playing deck: " + cards + "Deck of winnings: " + cardsWon; 
    }
}