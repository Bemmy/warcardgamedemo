package warcardgamedemo;

import java.util.ArrayList;
import java.util.List;

public class CardComparison {
    
    private Player player1;
    private Hand player1Hand;
    
    private Player player2;
    private Hand player2Hand;
    
    private List<Card> roundWinnings;
    
    public CardComparison(Player player1, Hand player1Hand, Player player2, Hand player2Hand){
        
        this.player1 = player1;
        this.player1Hand = player1Hand;
        
        this.player2 = player2;
        this.player2Hand = player2Hand;
    }
    
    public List<Card> getRoundWinnings(){
        return roundWinnings;
    }
    
    public Hand getPlayer1Hand(){
        return player1Hand;
    }
    
    public Hand getPlayer2Hand(){
        return player2Hand;
    }
    
    public void addCardsWon(Hand playerHand){
        
        if(playerHand.getPlayerWinningsDeck().getCardsWon().size() > 0){
            
        for(int i = 0; i < playerHand.getPlayerWinningsDeck().getCardsWon().size(); i++){
            
           playerHand.getPlayerDeck().getCards().add(playerHand.getPlayerWinningsDeck().getCardsWon().get(i));
        }
        
        playerHand.getPlayerWinningsDeck().getCardsWon().clear();
        
        }
    }
    
    public void shuffleHand(Hand playerHand){
        
         playerHand.getPlayerDeck().shuffleDeck(playerHand.getPlayerDeck());
    }
    
    public void addRoundWinnings(Hand playerHand){
        
          for (int i = 0; i < roundWinnings.size(); i++) {
              
             playerHand.getPlayerWinningsDeck().getCardsWon().add(roundWinnings.get(i));
         }   
          
          System.out.println("Winning Deck: " + playerHand.getPlayerWinningsDeck().getCardsWon().size());
    }
    
    public void chooseRoundWinner(Player player){
        
        WinnerFactory winnerFactory = WinnerFactory.getInstance();
        
        Winner roundWinner = (RoundWinner)(winnerFactory.getWinner(WinnerType.ROUNDWINNER, player));
         
        System.out.println(roundWinner);
            
        player.setRoundWins(player.getRoundWins() + 1);
    }
    
    public void chooseWarRoundWinner(Player player, Hand playerHand){
        
        WinnerFactory winnerFactory = WinnerFactory.getInstance();
         
        Winner warRoundWinner = 
                 (WarRoundWinner)(winnerFactory.getWinner(WinnerType.WARROUNDWINNER, player));
        
        System.out.println(warRoundWinner);
            
        for(int i = 0; i < roundWinnings.size(); i++){
                playerHand.getPlayerWinningsDeck().getCardsWon().add(roundWinnings.get(i));   
            }
        
        roundWinnings.clear();
              
        player.setWarRoundWins(player.getWarRoundWins()+ 1);
    }
    
    public void chooseGameWinner(Player player){
        
        WinnerFactory winnerFactory = WinnerFactory.getInstance();
       
        Winner gameWinner = (GameWinner)(winnerFactory.getWinner(WinnerType.GAMEWINNER, player));
        
        System.out.println(gameWinner);
    }
    
     public void collectPlayer1WarRoundCards(Hand player1Hand) { 
      
        player1Hand.getPlayerDeck().getWarHand().clear();
        
        System.out.println("\n collectPlayer1War: " + player1Hand.getPlayerDeck().getCards().size());
              
        player1Hand.callThreeCards();
                 
        for(int i = 0; i < player1Hand.getPlayerDeck().getWarHand().size(); i++){
           
            roundWinnings.add(player1Hand.getPlayerDeck().getWarHand().get(i));
        }
         
        }
    
     public void collectPlayer2WarRoundCards(Hand player2Hand) {
        
        player2Hand.getPlayerDeck().getWarHand().clear();
            
        System.out.println("\n collectPlayer2War: " + player2Hand.getPlayerDeck().getCards().size());
                  
        player2Hand.callThreeCards();
                 
        for(int i = 0; i < player2Hand.getPlayerDeck().getWarHand().size(); i++){
           
            roundWinnings.add(player2Hand.getPlayerDeck().getWarHand().get(i));
        }
        
    }
    
    public void compareAllGameRounds(Hand player1Hand, Hand player2Hand){
        
        boolean empty = false;
        
        roundWinnings = new ArrayList<Card>();
       
        while(empty != true){
   
            addCardsWon(player1Hand);    
            shuffleHand(player1Hand);       
        
            addCardsWon(player2Hand);
            shuffleHand(player2Hand);
        
           System.out.println("Round Begin: ");
           System.out.println("Player 1 Deck size: " + player1Hand.getPlayerDeck().getCards().size());
           System.out.println("Player 1 Winnings Deck size: " + player1Hand.getPlayerWinningsDeck().getCardsWon().size());
           System.out.println();

         System.out.println("Player 2 Deck size: " + player2Hand.getPlayerDeck().getCards().size());
         System.out.println("Player 2 Winnings Deck size: " + player2Hand.getPlayerWinningsDeck().getCardsWon().size());  
                      
            Card player1Card = null;
                
            if(player1Hand.getPlayerDeck().getCards().size() > 0){    
            
                player1Card = player1Hand.callOneCard();  
        
             }
        
            Card player2Card = null;
        
            if(player2Hand.getPlayerDeck().getCards().size() > 0){
            
                player2Card = player2Hand.callOneCard();
        
               
            } 

            if(player1Card != null && player2Card != null){
            
                System.out.println(player1Card + " vs. " + player2Card);
        
                roundWinnings.add(player1Card);
                roundWinnings.add(player2Card);
        
                int player1CardValue = player1Card.getValue().getIntValue();
                int player2CardValue = player2Card.getValue().getIntValue();
        
            if(player1CardValue == player2CardValue){
            
                addCardsWon(player1Hand);    
                shuffleHand(player1Hand);

                addCardsWon(player2Hand);    
                shuffleHand(player2Hand);        
            
                System.out.println("--------------   R  O  U  N  D        O  F        W  A  R   --------------");
            
            if(player1Hand.getPlayerDeck().getCards().size() < 3) {
                
               chooseGameWinner(player2);
               
               empty = true;
               
           } else if (player2Hand.getPlayerDeck().getCards().size() < 3){
               
               chooseGameWinner(player1);
               
               empty = true;
               
           } else {
            
           compareMultipleWarRounds();
           
        }
           
        } else if(player1CardValue > player2CardValue){
        
            addRoundWinnings(player1Hand);
    
            chooseRoundWinner(player1);
      
            roundWinnings.clear();
    
        } else {
             
            addRoundWinnings(player2Hand);
         
            chooseRoundWinner(player2);
        
            roundWinnings.clear();
        }  
        } else if (player1Hand.getPlayerDeck().getCards().size() == 0) { 
            
            chooseGameWinner(player2);
            
            empty = true;
            
        } else {
            
            chooseGameWinner(player1);
   
            empty = true;
        }
        
        }
    }
       
    public void compareMultipleWarRounds() {
       
        int player1Wins = 0;
        int player2Wins = 0;
        
        boolean tie = false;
       
        addCardsWon(player1Hand);
        
        addCardsWon(player2Hand);
        
        if(player1Hand.getPlayerDeck().getCards().size() >= 3 && player2Hand.getPlayerDeck().getCards().size() >= 3 ){
        
        while(tie != true){
            
            collectPlayer1WarRoundCards(player1Hand);
            
            collectPlayer2WarRoundCards(player2Hand);

            for(int i = 0; i < 3; i++){   
                 
                System.out.println("WAR: " + player1Hand.getPlayerDeck().getWarHand().get(i) + " vs. " + 
                        player2Hand.getPlayerDeck().getWarHand().get(i));
     
                if(player1Hand.getPlayerDeck().getWarHand().get(i).getValue().getIntValue() > 
                        player2Hand.getPlayerDeck().getWarHand().get(i).getValue().getIntValue()){
            
                    player1Wins++;
            
                    tie = true;
            
                } else if(player1Hand.getPlayerDeck().getWarHand().get(i).getValue().getIntValue() == 
                    player2Hand.getPlayerDeck().getWarHand().get(i).getValue().getIntValue()) {
            
                    if(player1Hand.getPlayerDeck().getCards().size() < 3){ 
                
                    chooseGameWinner(player2);
              
                
                } else if(player2Hand.getPlayerDeck().getCards().size() <3) { 
                
                    chooseGameWinner(player1);
            
                } else {
                    
                        tie = false;
                        continue;
                }
            
                } else {
            
                        player2Wins++;
            
                        tie = true;
                }
   
                if(player1Wins > player2Wins){
            
                    chooseWarRoundWinner(player1, player1Hand);
                    
                    addRoundWinnings(player1Hand);
                    
                    roundWinnings.clear();
                    
                } else {
            
                    chooseWarRoundWinner(player2, player2Hand);
                    
                     addRoundWinnings(player2Hand);
                     
                     roundWinnings.clear();
                }
            } 
        } 
   
    } else if (player1Hand.getPlayerDeck().getCards().size() < 3 ) { chooseGameWinner(player2); }
    else { chooseGameWinner(player1); }
    
}
}