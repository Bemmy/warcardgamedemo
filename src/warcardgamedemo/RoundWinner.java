package warcardgamedemo;

public class RoundWinner extends Winner {
    
    private Player player;
    
    public RoundWinner(){}
    
    public RoundWinner(Player player){
        super(player);
        this.player = player;
    }
    
    public Player getPlayer(){
        return player;
    }
    
    public String toString(){
        
        return "Round Winner is: " + player.getUserName();
    }
}