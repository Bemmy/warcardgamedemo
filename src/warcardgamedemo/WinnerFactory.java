package warcardgamedemo;

public class WinnerFactory {
    
    private static WinnerFactory winnerFactory;
    
    public WinnerFactory(){
        this.winnerFactory = winnerFactory;
    }
    
    public static WinnerFactory getInstance(){
        
        if (winnerFactory == null){
           
            winnerFactory = new WinnerFactory(); 
        }
                    return winnerFactory;
    }
    
    public Winner getWinner(WinnerType type, Player player){
        
        Winner w = null;
        
        switch(type) {
                                      
            case GAMEWINNER:
             w = new GameWinner(player);
             break;
            case WARROUNDWINNER: 
             w = new WarRoundWinner(player);
             break; 
            case ROUNDWINNER:
             w = new RoundWinner(player);
             break;
        }
        
        return w;
    }
}