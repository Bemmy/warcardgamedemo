package warcardgamedemo;

public class WarRoundWinner extends Winner {
    
private Player player;
        
    public WarRoundWinner(Player player){
        super(player);
        this.player = player;
    }
    
    public Player getPlayer(){
        return player;
    }
    
    public String toString(){
        
        return "War Round Winner is: " + player.getUserName();
    }  
}