package warcardgamedemo;

public class WarCardGame {
    
    private Player player1;
    private Player player2;
    
    public WarCardGame(Player player1, Player player2){
        this.player1 = player1;
        this.player2 = player2;
    }
    
    public void playWarCardGame(){
        
                Deck gameDeck = new Deck();
                gameDeck.shuffleDeck(gameDeck);  
                
                Deck player1Deck = new Deck();
                Deck player2Deck = new Deck();
                Hand gameHand = new Hand();

                gameHand.splitGameDeckInto2DeckHands(gameDeck, player1Deck, player2Deck);
                                
                Hand player1Hand = new Hand(player1Deck);
                
                Hand player2Hand = new Hand(player2Deck);
                
                CardComparison compareUnit = new CardComparison(player1, player1Hand, player2, player2Hand);
                
                compareUnit.compareAllGameRounds(player1Hand, player2Hand);
                
                ScoreBoard scoreBoard = new ScoreBoard(player1, player2);
                
                System.out.println(scoreBoard);
            }
 }
    

