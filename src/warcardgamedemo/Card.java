package warcardgamedemo;

public class Card {
    
    private CardSuit suit;
    private CardValue value;
    
    public Card(CardSuit suit, CardValue value){
        this.suit = suit;
        this.value = value;
    }
    
    public CardValue getValue(){
        return value;
    }
    
    public CardSuit getSuit(){
        return suit;
    }
    
    public String toString(){
        return  value.name() + " of " + suit.name();
    }  
}
