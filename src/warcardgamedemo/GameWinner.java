package warcardgamedemo;

public class GameWinner extends Winner {
    
    private Player player;
    
    public GameWinner(Player player){
        super(player);
        this.player = player;
    }
    
    public Player getPlayer(){
        return player;
    }
    
    public String toString(){
        
        return "\n\n  ********** G A M E  W I N N E R ********** \n\n" + "\t" + player.getUserName() ;
    }
}