package warcardgamedemo;

public class Player {
    
    private String username;
    private int roundWins;
    private int warRoundWins;
    
    public Player(String username, int roundWins, int warRoundWins){
        this.username = username;
        this.roundWins = roundWins;
        this.warRoundWins = warRoundWins;
    }
    
    public String getUserName(){
        return username;
    }
    
   public int getRoundWins(){
        return roundWins;
    }
    
     public int getWarRoundWins(){
        return warRoundWins;
    }
     
     public void setRoundWins(int roundWins){
        this.roundWins = roundWins;
    }
    
     public void setWarRoundWins(int warRoundWins){
        this.warRoundWins = warRoundWins;
    }
    
}