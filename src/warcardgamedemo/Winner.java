package warcardgamedemo;

public class Winner {
   
    private Player player;
   
    public Winner(){}
    
    public Winner(Player player){
        this.player = player;
    }
    
    public Player getPlayer(){
        return player;
    }
}